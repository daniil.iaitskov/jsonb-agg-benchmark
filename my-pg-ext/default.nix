{
  pkgs ? import <nixpkgs> {}
}:
pkgs.stdenv.mkDerivation rec {
  pname = "my-pg-ext";
  version = "0.1.0";

  src = ./.;

  buildInputs = [
    pkgs.gcc pkgs.postgresql_14
  ];

  PG14 = "${pkgs.postgresql_14}/include";

  configurePhase = ''
    echo My Configuration Phase. $gcc
    gcc --version
  '';

  # buildPhase = ''
  #   gcc -fPIC -c my-pg-ext.c
  #   gcc -shared -o my-pg-ext.so my-pg-ext.o
  # ';'
  # installPhase = ''
  #   # mkdir -p $out/bin
  #   # cp my-pg-ext.so $out/bin
  # '';
}
