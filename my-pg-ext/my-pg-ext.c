#include "postgres.h"
#include <string.h>
#include "fmgr.h"
#include "utils/geo_decls.h"
#include "utils/jsonb.h"
#include "utils/numeric.h"
#include "utils/elog.h"
#include "server/catalog/pg_type_d.h"

PG_MODULE_MAGIC;

// uncomment for debugging #define myelog(...) (elog(DEBUG, __VA_ARGS__))
#define myelog(...)

typedef uint64 HashKey;
typedef int32 HashVal;

typedef struct {
  HashKey key;
  HashVal val;
} HashEntry;

typedef struct {
  Size capacity;
  HashEntry entries[0];
} IntHash;

static inline IntHash* newIntHash(Size);
static inline Size nextCapacity(Size);
static inline IntHash* doubleIntHashCapacity(IntHash*);
static IntHash * addHashValue(IntHash*, HashKey, HashVal);
static inline IntHash* addJsonbToIntHash(IntHash*, Jsonb*);
static inline Jsonb* intHashToJsonb(IntHash*);
static inline JsonbParseState * clone_parse_state(JsonbParseState *);

static inline IntHash* newIntHash(Size initCapacity) {
  IntHash* r = (IntHash*) palloc0(sizeof(IntHash) + sizeof(HashEntry) * initCapacity);
  r->capacity = initCapacity;
  myelog("newIntHash: capacity %lu", initCapacity);
  return r;
}

static Size primes[] =
  {
   1,1,2,3,5,5,7,7,11,11,11,11,13,13,17,17,17,17,19,19,23,23,23,23,29,29,29,29,29,29,31,31,37,37,37,37,37,37,
   41,41,41,41,43,43,47,47,47,47,53,53,53,53,53,53,59,59,59,59,59,59,61,61,67,67,67,67,67,67,71,71,71,71,
   73,73,79,79,79,79,79,79,83,83,83,83,89,89,89,89,89,89,97,97,97,97,97,97,97,97,101,101,101,101,103,103,
   107,107,107,107,109,109,113,113,113,113,127,127,127,127,127,127,127,127,127,127,127,127,127,127,
   131,131,131,131,137,137,137,137,137,137,139,139,149,149,149,149,149,149,149,149,149,149,151,151,
   157,157,157,157,157,157,163,163,163,163,163,163,167,167,167,167,173,173,173,173,173,173,179,179,179,179,179,179,
   181,181,191,191,191,191,191,191,191,191,191,191,193,193,197,197,197,197,199,199,
   211,211,211,211,211,211,211,211,211,211,211,211,223,223,223,223,223,223,223,223,223,223,223,223,227,227,227,227,229,229,
   233,233,233,233,239,239,239,239,239,239,241,241,251,251,251,251,251,251,251,251,251,251,257,257,257,257,257,257,
   263,263,263,263,263,263,269,269,269,269,269,269,271,271,277,277,277,277,277,277,
   281,281,281,281,283,283,293,293,293,293,293,293,293,293,293,293
  };

static inline Size nextCapacity(Size currentCap) {
  const Size nextCap = currentCap * 2;
  if (nextCap >= sizeof(primes) / sizeof(primes[0])) {
    return nextCap;
  }
  return primes[nextCap];
}

#define IsBusyCell(k) (k)
#define CellKeySize(k) ((int) *(char*) &k)

static inline IntHash* doubleIntHashCapacity(IntHash* ih) {
  const Size newCap = nextCapacity(ih->capacity);
  IntHash* doubledTable = newIntHash(newCap);
  HashEntry* e = ih->entries;
  Size left = ih->capacity;
  myelog("doubleIntHashCapacity: expand capacity %lu => %lu", left, newCap);
  while (left--) {
    if (IsBusyCell(e->key)) {
      doubledTable = addHashValue(doubledTable, e->key, e->val);
    }
    ++e;
  }
  pfree(ih);
  return doubledTable;
}

#define DOUBLE_TABLE_CAPACITY_TRIES 3

static IntHash* addHashValue(IntHash* ih, HashKey key, HashVal val) {
  int tries = DOUBLE_TABLE_CAPACITY_TRIES;
  Size busy_step = 1;
  Size arrIdx = key % ih->capacity;
  while (true) {
    if (ih->entries[arrIdx].key) {
      if (ih->entries[arrIdx].key == key) {
        myelog("addHashValue: merge %lx %d with %d at idx %lu", key, val, ih->entries[arrIdx].val, arrIdx);
        ih->entries[arrIdx].val += val;
        return ih;
      } else {
        myelog("addHashValue: idx %lu is busy with %lx %d", arrIdx, ih->entries[arrIdx].key, ih->entries[arrIdx].val);
      }
    } else {
      myelog("addHashValue: use idx %lu for %lx %d", arrIdx, key, val);
      ih->entries[arrIdx].key = key;
      ih->entries[arrIdx].val = val;
      return ih;
    }
    if ((arrIdx += busy_step++) >= ih->capacity) {
      myelog("addHashValue: arrIdx overflow %lu", arrIdx);
      arrIdx %= ih->capacity; // start
    }
    if (!--tries) {
      ih = doubleIntHashCapacity(ih);
      busy_step = 1;
      tries = DOUBLE_TABLE_CAPACITY_TRIES;
    }
  }
}

static inline IntHash* addJsonbToIntHash(IntHash* ih, Jsonb* jb) {
  JsonbValue curVal;
  JsonbIteratorToken itrTok;
  JsonbIterator* itr = JsonbIteratorInit(&jb->root);
  HashKey lastKey = 0;
  bool err;
  if (!ih) {
    Size cap = JB_ROOT_COUNT(jb) * 10 / 4 /* 75% capacity is used */;
    if (cap)
      ih = newIntHash(cap);
    else
      return ih;
  }
  itrTok = JsonbIteratorNext(&itr, &curVal, false);
  while (1) {
    myelog("addJsonbToIntHash token %d", itrTok);
    if (itrTok == WJB_DONE) {
      myelog("addJsonbToIntHash DONE");
      break;
    } else if (itrTok == WJB_KEY) {
      if (curVal.val.string.len < sizeof(HashKey)) {
        *(char*) &lastKey = (char) curVal.val.string.len;
        memcpy(((char*) &lastKey) + 1, curVal.val.string.val, curVal.val.string.len);

      } else {
        lastKey = 0;
        myelog("addJsonbToIntHash: skip long key %d; type is %d", curVal.val.string.len, curVal.type);
      }
    } else if (itrTok == WJB_VALUE && IsBusyCell(lastKey)) {
      if (curVal.type == jbvNumeric) {
        HashVal val = numeric_int4_opt_error(curVal.val.numeric, &err);
        if (err) {
          const char* numAsStr = numeric_normalize(curVal.val.numeric);
          ereport(ERROR,
                  (errcode(ERRCODE_NUMERIC_VALUE_OUT_OF_RANGE),
                   errmsg("integer out of 32bit range: %s", numAsStr)));
        }

        myelog("addJsonbToIntHash add 0x%lx %d", lastKey, val);
        ih = addHashValue(ih, lastKey, val);
      } else {
        myelog("addJsonbToIntHash: skip not numeric value; type is %d", curVal.type);
        lastKey = 0;
      }
    } else {
      myelog("addJsonbToIntHash: skip itr tor %d 0x%lx", itrTok, lastKey);
    }
    itrTok = JsonbIteratorNext(&itr, &curVal, true);
  }
  return ih;
}

static inline Jsonb* intHashToJsonb(IntHash* ih) {
  Jsonb* out;
  JsonbParseState* parseState = 0;
  JsonbValue* jKey;  JsonbValue* jVal;
  HashKey hKey;
  int len;
  JsonbValue* jsonHead = pushJsonbValue(&parseState, WJB_BEGIN_OBJECT, NULL);
  HashEntry* hEntry = ih->entries;

  Size arrIdx = ih->capacity;
  Size filledEntries = 0;

  myelog("intHashToJsonb iterate %lu slots", ih->capacity);
  while (arrIdx--) {
    if (hKey = hEntry->key) {
      ++filledEntries;
      len = CellKeySize(hKey);
      myelog("intHashToJsonb push key 0x%lx with length %d", hKey, len);

      jKey = (JsonbValue*) palloc(sizeof(JsonbValue));
      jKey->type = jbvString;
      jKey->val.string.len = len;
      jKey->val.string.val = palloc(len);
      (void) memcpy(jKey->val.string.val, ((char*) &hKey) + 1, len);
      (void) pushJsonbValue(&parseState, WJB_KEY, jKey);

      jVal = (JsonbValue*) palloc(sizeof(JsonbValue));
      jVal->type = jbvNumeric;
      jVal->val.numeric = int64_to_numeric(hEntry->val);
      (void) pushJsonbValue(&parseState, WJB_VALUE, jVal);

      myelog("intHashToJsonb push val %d", hEntry->val);
    }
    ++hEntry;
  }

  myelog("intHashToJsonb iterated %lu discovered", filledEntries);
  parseState = clone_parse_state(parseState);
  jsonHead = pushJsonbValue(&parseState, WJB_END_OBJECT, NULL);
  out = JsonbValueToJsonb(jsonHead);
  pfree(ih);
  myelog("intHashToJsonb return");
  return out;
}

typedef struct { IntHash* result; } JsonbIntMapAgg;

PG_FUNCTION_INFO_V1(jsonb_int_map_agg_transfn);
Datum
jsonb_int_map_agg_transfn(PG_FUNCTION_ARGS) {
  MemoryContext oldcontext, aggcontext;
  JsonbIntMapAgg* state;

  if (!AggCheckCallContext(fcinfo, &aggcontext)) {
    elog(ERROR, "jsonb_int_map_agg_transfn called in non-aggregate context");
  }

  if (PG_ARGISNULL(0)) {
    myelog("jsonb_int_map_agg_transfn new");
    oldcontext = MemoryContextSwitchTo(aggcontext);
    state = palloc0(sizeof(JsonbIntMapAgg));

    MemoryContextSwitchTo(oldcontext);
  } else {
    state = (JsonbIntMapAgg*) PG_GETARG_POINTER(0);
    if (state->result) {
      myelog("jsonb_int_map_agg_transfn continue %lu capacity", state->result->capacity);
    } else {
      myelog("jsonb_int_map_agg_transfn continue empty capacity");
    }
  }

  if (PG_ARGISNULL(1))
    ereport(ERROR, (errcode(ERRCODE_INVALID_PARAMETER_VALUE), errmsg("field name must not be null")));

  if (get_fn_expr_argtype(fcinfo->flinfo, 1) != JSONBOID)
    ereport(ERROR, (errcode(ERRCODE_INVALID_PARAMETER_VALUE), errmsg("jsonb is only supported input data type")));

  oldcontext = MemoryContextSwitchTo(aggcontext);
  state->result = addJsonbToIntHash(state->result, PG_GETARG_JSONB_P(1));
  MemoryContextSwitchTo(oldcontext);

  if (state->result) {
    myelog("jsonb_int_map_agg_transfn ret %lu capacity", state->result->capacity);
  } else {
    myelog("jsonb_int_map_agg_transfn ret empty capacity");
  }

  PG_RETURN_POINTER(state);
}

PG_FUNCTION_INFO_V1(jsonb_int_map_agg_finalfn);
Datum
jsonb_int_map_agg_finalfn(PG_FUNCTION_ARGS) {
  JsonbIntMapAgg* arg;
  JsonbParseState* parseState;
  JsonbValue* res;

  Assert(AggCheckCallContext(fcinfo, NULL));
  if (PG_ARGISNULL(0)) PG_RETURN_NULL();
  arg = (JsonbIntMapAgg*) PG_GETARG_POINTER(0);

  PG_RETURN_POINTER(intHashToJsonb(arg->result));
}

/*
create or replace function jsonb_int_map_agg_transfn(internal, jsonb) returns internal language c as '/home/postgres/my-pg-ext.so', 'jsonb_int_map_agg_transfn';
create or replace function jsonb_int_map_agg_finalfn(internal) returns jsonb language c as '/home/postgres/my-pg-ext.so', 'jsonb_int_map_agg_finalfn';
create or replace aggregate sum_jsonb_object_values (jsonb)
(
  sfunc = jsonb_int_map_agg_transfn,
  stype = internal,
  finalfunc = jsonb_int_map_agg_finalfn
);

drop function sum_jsonb_int_vals;
create or replace function sum_jsonb_int_vals(jsonb) returns int language c as '/home/postgres/my-pg-ext.so', 'sum_jsonb_int_vals';
select sum_jsonb_int_vals('{"k": 1}');
*/
PG_FUNCTION_INFO_V1(sum_jsonb_int_vals);
Datum
sum_jsonb_int_vals(PG_FUNCTION_ARGS)
{
  int32 r = 0;
  bool err = 0;

  Jsonb * jm = PG_GETARG_JSONB_P(0);

  if (jm && (jm->root.header & JB_FOBJECT)) {
    myelog("sum_jsonb_int_vals got an object");
    JsonbValue curVal;
    JsonbIteratorToken itrTok;
    JsonbIterator * itr = JsonbIteratorInit(&jm->root);

    itrTok = JsonbIteratorNext(&itr, &curVal, false);
    while (1) {
      myelog("sum_jsonb_int_vals current iteration %d", itrTok);
      if (itrTok == WJB_DONE) {
        myelog("sum_jsonb_int_vals DONE iteration");
        break;
      } else if (itrTok == WJB_VALUE) {
        myelog("sum_jsonb_int_vals check cur val %d", curVal.type);
        if (curVal.type == jbvNumeric) {
          int d = numeric_int4_opt_error(curVal.val.numeric, &err);
          myelog("sum_jsonb_int_vals add %d", d);
          r += d;
        }
      }
      itrTok = JsonbIteratorNext(&itr, &curVal, true);
    }
  } else {
    myelog("sum_jsonb_int_vals got not object");
  }

  PG_RETURN_INT32(r);
}


/******************************************
 * static copies from jsonb.c *
 *****************************************/

/*
 * shallow clone of a parse state, suitable for use in aggregate
 * final functions that will only append to the values rather than
 * change them.
 */
static inline JsonbParseState * clone_parse_state(JsonbParseState *state) {
  JsonbParseState *result, *icursor, *ocursor;

  if (state == NULL) return NULL;

  result = palloc(sizeof(JsonbParseState));
  icursor = state;
  ocursor = result;
  for (;;)
  {
    ocursor->contVal = icursor->contVal;
    ocursor->size = icursor->size;
    icursor = icursor->next;
    if (icursor == NULL)
      break;
    ocursor->next = palloc(sizeof(JsonbParseState));
    ocursor = ocursor->next;
  }
  ocursor->next = NULL;

  return result;
}
