create or replace function json_apply(lambda text, arg jsonb)
  returns jsonb
  immutable
  language plpgsql
  as $$ declare x jsonb;
  begin
    execute ('select ' || lambda) into x using arg ; return x;
  end
  $$ ;

create or replace function jmkeys(m jsonb)
  returns text[]
  immutable
  language sql
  as $$ select array(select jsonb_object_keys(m)) $$ ;

create or replace function fmap_jsonb_vals(lambda text, m jsonb)
  returns jsonb
  immutable
  language plpgsql
  as $$
  declare
    k text;
  begin
    foreach k in array jmkeys(m) loop
      m := jsonb_set(m, array[k], json_apply(lambda, m -> k));
    end loop;
    return m;
  end
  $$ ;

create or replace function merge_keys_sum_values(st jsonb, v jsonb)
  returns jsonb
  immutable
  parallel safe
  language plpgsql
  as $$
  declare
    vkey text;
    stval jsonb;
  begin
    if st is null then
      return v;
    end if;
    foreach vkey in array jmkeys(v) loop
      stval := st -> vkey;
      if stval is null then
        st := jsonb_set(st, array[vkey], v -> vkey);
      else
        st := jsonb_set(st, array[vkey], to_jsonb(((v -> vkey) :: int + (st -> vkey) :: int)));
      end if;
    end loop;
    return st;
  end
  $$ ;

create or replace aggregate agg_int_map (jsonb)
(
  sfunc = merge_keys_sum_values,
  stype = jsonb
);

drop table if exists int_map_jsonb;
create table int_map_jsonb (c jsonb);

create or replace function insert_into_jsonb_table(ncols int, nrows int)
  returns void
  language sql
  as $BODY$
    truncate int_map_jsonb;
    with
      nc as (select v from generate_series(1, ncols) v),
      nr as (select v from generate_series(1, nrows) v),
      json_obj(jo) as
        (select
          fmap_jsonb_vals(
            $$ jsonb_extract_path_text($1, VARIADIC (array[] :: text[])) :: int $$, -- values are creates as json strings
            jsonb_object(
              array(select 'c_' || v from nc v),
              array(select '1' from nc))))
    insert into int_map_jsonb select jo from nr, json_obj ;
  $BODY$ ;

create or replace function define_wide_table (tname text, ncols int, coltype text)
  returns void
  language plpgsql
  as $$
  declare
    columns text := (select string_agg('c_' || v || ' ' || coltype, ', ') from generate_series(1, ncols) v);
  begin
    execute ('create table ' || tname || '(' || columns || ')');
  end
  $$ ;

create or replace function insert_wide_table (tname text, ncols int, nrows int)
  returns void
  language plpgsql
  as $$
  declare
    i int ;
    colvals text := (select string_agg('1', ', ') from generate_series(1, ncols) v);
  begin
    execute ('truncate ' || tname);
    for i in 0..nrows loop
      execute ('insert into ' || tname || ' values(' || colvals || ')');
    end loop;
  end
  $$ ;

create or replace function agg_wide_table (tname text, ncols int, aggf text)
  returns int
  language plpgsql
  as $$
  declare
    res int;
    colaggs text := (select string_agg(aggf || '(c_' || v || ')', ' + ') from generate_series(1, ncols) v);
  begin
    execute ('select ' || colaggs  || ' from ' || tname) into res;
    return res;
  end
  $$ ;

----------------------------
\echo Begin Benchmark

select insert_into_jsonb_table(100, 100);
\echo Aggregating table of jsonb column 100cx100r
explain analyze select agg_int_map(c) from int_map_jsonb;

drop table if exists wide_table_100;
select define_wide_table('wide_table_100', 100, 'int');
select insert_wide_table('wide_table_100', 100, 100);
\echo Aggregating table of int columns 100cx100r
explain analyze select agg_wide_table('wide_table_100', 100, 'sum');


select insert_into_jsonb_table(50, 100);
\echo Aggregating table of jsonb column 50cx100r
explain analyze select agg_int_map(c) from int_map_jsonb;

select insert_into_jsonb_table(25, 100);
\echo Aggregating table of jsonb column 25cx100r
explain analyze select agg_int_map(c) from int_map_jsonb;

drop table if exists wide_table_25;
select define_wide_table('wide_table_25', 25, 'int');
select insert_wide_table('wide_table_25', 25, 100);
\echo Aggregating table of int columns 25cx100r
explain analyze select agg_wide_table('wide_table_25', 25, 'sum');
\echo End Benchmark


select insert_into_jsonb_table(25, 10000);
\echo Aggregating table of jsonb column 25cx10000r
explain analyze select agg_int_map(c) from int_map_jsonb;

drop table if exists wide_table_100;
select define_wide_table('wide_table_10000', 25, 'int');
select insert_wide_table('wide_table_10000', 25, 10000);
\echo Aggregating table of int columns 25cx10000r
explain analyze select agg_wide_table('wide_table_10000', 25, 'sum');
\echo End Benchmark


---------------------
-- jsonb agg through foreach + -> + jsonb_set is slower:
--  (117/14ms = 9 times) for 100 columns and 100 rows
--  (11ms/3.338ms = 3 times) for 25 columns and 100 rows
--  (1000ms/8ms = 100 times) for 25 columns and 10000 rows
